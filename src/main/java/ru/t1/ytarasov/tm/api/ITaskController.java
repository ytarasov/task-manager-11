package ru.t1.ytarasov.tm.api;

import ru.t1.ytarasov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTask();

    void showTasks();

    void showTask(Task task);

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();
}
