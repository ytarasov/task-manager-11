package ru.t1.ytarasov.tm.api;

import ru.t1.ytarasov.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeById();

    void removeByIndex();
}
